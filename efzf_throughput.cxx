#include <etherfabric/capabilities.h>
#include <etherfabric/checksum.h>
#include <etherfabric/memreg.h>
#include <etherfabric/pio.h>
#include <etherfabric/vi.h>
#include <etherfabric/pd.h>
#include <ci/tools.h>
#include <zf/zf.h>

#include <ci/driver/efab/hardware/byteswap.h>
#include <ci/driver/efab/hardware/host_ef10_common.h>
#include <ci/efhw/hardware_sysdep.h>
#include <ci/tools/bitfield.h>

#include <arpa/inet.h>
#include <cstddef>
#include <ifaddrs.h>
#include <linux/if_ether.h>
#include <net/if.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <x86intrin.h>

#include <array>
#include <boost/container/static_vector.hpp>
#include <tuple>
#include <vector>

#include "utils.h"

inline constexpr uint32_t tx_buf_size = 512; // 8 per page
inline constexpr uint32_t no_interfaces = 2;
inline constexpr uint32_t no_vis_per_interface = 4;
inline constexpr uint32_t no_bufs_per_vi = 8; // 4096/512

inline constexpr uint32_t no_bufs_total =
    no_interfaces * no_vis_per_interface * no_bufs_per_vi;
static_assert(no_bufs_total == 64);

struct vi_pio_memreg {
    ef_vi vi{};
    ef_pio pio{};
    char* tx_mem{};
    ef_memreg memreg{};
};

struct interface {
    const char* if_name;
    int if_index{};
    uint32_t resource_id{};

    ef_pd pd{};

    addrinfo* ai{};
    zf_attr* attr{};
    zf_stack* stack{};

    ef_vi *last_used_vi{};

    boost::container::static_vector<vi_pio_memreg, no_vis_per_interface> vis{};
};

inline auto split_resource_id(const uint32_t id) noexcept {
  return std::pair<uint32_t, uint32_t>{
      (id / no_bufs_per_vi) % no_vis_per_interface, // vi identifier
      (id % no_bufs_per_vi)}; // buffer within vi identifier
}

inline constexpr uint32_t no_connections = 6;
inline constexpr uint32_t msg_len = 300;

inline constexpr uint32_t IP_TCP_HEADERS = 20/*IP*/ + 20/*TCP*/; // TCP options not supported

typedef ci_qword_t ef_vi_ef10_dma_tx_buf_desc;
typedef ci_qword_t ef_vi_ef10_dma_tx_phys_desc;

inline void ef10_pio_set_desc(ef_vi *vi, ef_vi_txq *q, ef_vi_txq_state *qs,
                              int offset, int len, ef_request_id dma_id) {
  ef_vi_ef10_dma_tx_buf_desc *dp;
  unsigned di = qs->added & q->mask;

  dp = (ef_vi_ef10_dma_tx_buf_desc *)q->descriptors + di;

  CI_POPULATE_QWORD_4(*dp, ESF_DZ_TX_PIO_TYPE, 1, ESF_DZ_TX_PIO_OPT, 1,
                      ESF_DZ_TX_PIO_BYTE_CNT, len, ESF_DZ_TX_PIO_BUF_ADDR,
                      offset);

  q->ids[di] = dma_id;
}

inline int ef10_tx_descriptor_can_push(const ci_qword_t *dp) {
  const uint64_t is_opt = (uint64_t)1u << ESF_DZ_TX_DESC_IS_OPT_LBN;
  const uint64_t is_cont = (uint64_t)1u << ESF_DZ_TX_USR_CONT_LBN;
  const uint64_t is_pio = (uint64_t)1u << ESF_DZ_TX_PIO_OPT_LBN;

  return (((dp->u64[0] & (is_opt | is_cont)) == 0) ||
          ((dp->u64[0] & (is_opt | is_pio)) == (is_opt | is_pio)));
}

inline void ef_vi_transmit_push_doorbell(ef_vi *vi) {
  uint32_t *doorbell =
      reinterpret_cast<uint32_t *>(vi->io + ER_DZ_TX_DESC_UPD_REG);
  writel(vi->ep_state->txq.added & vi->vi_txq.mask,
         reinterpret_cast<volatile char *>(doorbell + 2));
  mmiowb();
}

inline void ef_vi_transmit_push_desc(ef_vi *vi,
                                     ef_vi_ef10_dma_tx_phys_desc *dp) {
  ef_vi_txq *q = &vi->vi_txq;
  ef_vi_txq_state *qs = &vi->ep_state->txq;
  uint32_t *dbell =
      reinterpret_cast<uint32_t *>(vi->io + ER_DZ_TX_DESC_UPD_REG);

  ci_oword_t d;
  d.u32[0] = dp->u32[0];
  d.u32[1] = dp->u32[1];
  d.u32[2] = cpu_to_le32(qs->added & q->mask);

  __asm__ __volatile__("movups %1, %%xmm0\n\t"
                       "movaps %%xmm0, %0"
                       : "=m"(*(volatile uint64_t *)(dbell))
                       : "m"(d)
                       : "xmm0", "memory");
  mmiowb();
}

inline void ef10_ef_vi_transmit_push(ef_vi *vi) {
  ef_vi_txq_state *qs = &vi->ep_state->txq;
  ef_vi_txq *q = &vi->vi_txq;
  unsigned di = qs->previous & q->mask;
  ef_vi_ef10_dma_tx_phys_desc *dp =
      (ef_vi_ef10_dma_tx_buf_desc *)q->descriptors + di;

  if ((qs->previous - qs->removed) < vi->tx_push_thresh &&
      ef10_tx_descriptor_can_push(dp))
    ef_vi_transmit_push_desc(vi, dp);
  else
    ef_vi_transmit_push_doorbell(vi);

  qs->previous = qs->added;
}

inline void ef10_pio_push(ef_vi *vi, ef_vi_txq_state *qs) {
  /* Prior call to ef10_pio_set_desc does not increment queue position as
   * it is used for pio send path warming.  Increment here before push.
   */
  qs->added++;

  /* Ensure that the descriptor writes and doorbell go out in order.  There is
   * no need for wmb_wc() here as that was done after the PIO write.
   */
  wmb();
  ef10_ef_vi_transmit_push(vi);
}

inline void ef10_pio_push(ef_vi *vi) { ef10_pio_push(vi, &vi->ep_state->txq); }

inline int ef10_ef_vi_transmit_pio_no_push(ef_vi *vi, int offset, int len,
                                           ef_request_id dma_id) {
  ef_vi_txq *q = &vi->vi_txq;
  ef_vi_txq_state *qs = &vi->ep_state->txq;

  if (CI_UNLIKELY((offset & 63) != 0))
    return -EINVAL;

  ef10_pio_set_desc(vi, q, qs, offset, len, dma_id);

  if (qs->added - qs->removed < q->mask) {
    ef10_pio_push(vi, qs);
    return 0;
  } else {
    /* Undo effect of ef10_pio_set_desc */
    q->ids[qs->added & q->mask] = EF_REQUEST_ID_MASK;
    return -EAGAIN;
  }
}

struct connection {
    zft_handle* handle{};
    zft* zock{};

    zf_ds zfds{};
    uint32_t interface_id{};
    uint32_t resource_id{};
    uint32_t pio_pkt_len{};

    char* data() {
        assert(zfds.headers && zfds.headers_len == ETH_HLEN + IP_TCP_HEADERS);
        return reinterpret_cast<char*>(zfds.headers) + zfds.headers_len;
    }
};

template <typename I>
inline uint32_t smart_transmit(connection &conn, I &interfaces) {
  if (!conn.pio_pkt_len)
    return 0;

  auto &intf = interfaces[conn.interface_id];
  const auto [vi_id, buf_id] = split_resource_id(conn.resource_id);
  auto &vi = intf.vis[vi_id];

  // TRY(ef_vi_transmit_pio(&vi.vi, buf_id * tx_buf_size, conn.pio_pkt_len, 0));
  TRY(ef10_ef_vi_transmit_pio_no_push(&vi.vi, buf_id * tx_buf_size,
                                      conn.pio_pkt_len, 0));

  /* if (intf.last_used_vi && intf.last_used_vi != &vi.vi)
      ef10_pio_push(intf.last_used_vi);

  TRY(ef10_ef_vi_transmit_pio_no_push(&vi.vi, buf_id * tx_buf_size,
  conn.pio_pkt_len, 0)); intf.last_used_vi = &vi.vi; */

  return 1;
}

int main(int argc, char *argv[]) {
    [[maybe_unused]] bool ctpio_tx = false;
    bool immediate_complete = false;

    for (int c; (c = getopt(argc, argv, "ci")) >= 0;) {
        switch(c) {
            case 'c':
                ctpio_tx = true;
                break;
            case 'i':
                immediate_complete = true;
                break;
        }
    }

    ef_driver_handle dh;
    TRY(ef_driver_open(&dh));
    TRY(zf_init());

    boost::container::static_vector<interface, no_interfaces> interfaces{
        {"sf_arista"}, {"sf_mm"}};
    TRY(getaddrinfo("10.9.58.42",  "30000", nullptr, &interfaces[0].ai));
    TRY(getaddrinfo("10.31.62.42", "30000", nullptr, &interfaces[1].ai));

    for (auto& intf : interfaces) {
        intf.if_index = if_nametoindex(intf.if_name);

        if (!intf.if_index) {
            fprintf(stderr, "failed if_nametoindex: [%s]\n", intf.if_name);
            abort();
        }

        TRY(ef_pd_alloc(&intf.pd, dh, intf.if_index, EF_PD_DEFAULT));

        for (uint32_t i = 0; i < no_vis_per_interface; ++i) {
            auto& vi = intf.vis.emplace_back();

            TRY(ef_vi_alloc_from_pd(&vi.vi, dh, &intf.pd, dh, -1, -1,-1, nullptr, -1, EF_VI_FLAGS_DEFAULT));
            TRY(ef_pio_alloc(&vi.pio, dh, &intf.pd, -1, dh));
            TRY(ef_pio_link_vi(&vi.pio, dh, &vi.vi, dh));

            void* tx_mem{};
            TEST(posix_memalign(&tx_mem, CI_PAGE_SIZE, CI_PAGE_SIZE) == 0);
            TRY(ef_memreg_alloc(&vi.memreg, dh, &intf.pd, dh, tx_mem, CI_PAGE_SIZE));
            vi.tx_mem = reinterpret_cast<char*>(tx_mem);
        }

        TRY(zf_attr_alloc(&intf.attr));
        TRY(zf_attr_set_str(intf.attr, "interface", intf.if_name));
        TRY(zf_attr_set_int(intf.attr, "reactor_spin_count", 1));
        TRY(zf_stack_alloc(intf.attr, &intf.stack));
    }

    boost::container::static_vector<connection, no_connections> connections{};
    uint32_t no_connected{};

    for (uint32_t i = 0; i < no_connections; ++i) {
      auto &conn = connections.emplace_back();

      // attach connection to interface (round robin)
      conn.interface_id = i % no_interfaces;
      auto &intf = interfaces[conn.interface_id];

      TRY(zft_alloc(intf.stack, intf.attr, &conn.handle));
      TRY(zft_connect(conn.handle, intf.ai->ai_addr, intf.ai->ai_addrlen,
                      &conn.zock));

      while (zft_state(conn.zock) == TCP_SYN_SENT)
        zf_reactor_perform(intf.stack);

      TEST(zft_state(conn.zock) == TCP_ESTABLISHED);
      ++no_connected;
    }

    fprintf(stderr, "connected: [%u]\n", no_connected);

    for (uint32_t i = 0; i < 16; ++i) {
      for (int m = 0; m < 1000; ++m) {
        // poll all interfaces
        for (auto &intf : interfaces) {
          zf_reactor_perform(intf.stack);

          ef_event evs[4]{};
          const uint32_t max_evs = 4;
          ef_request_id tx_ids[EF_VI_TRANSMIT_BATCH]{};

          // poll all vi's on interface
          for (auto &vi : intf.vis) {
            const int ev_n = ef_eventq_poll(&vi.vi, evs, max_evs);

            for (int e = 0; e < ev_n; ++e) {
              if (EF_EVENT_TYPE(evs[e]) == EF_EVENT_TYPE_TX) {
                [[maybe_unused]] const int n_tx =
                    ef_vi_transmit_unbundle(&vi.vi, &evs[e], tx_ids);
                // fprintf(stderr, "ef_vi_transmit_unbundle: [%d]\n", n_tx);
              } else {
                fprintf(stderr, "unexpected event " EF_EVENT_FMT "\n",
                        EF_EVENT_PRI_ARG(evs[e]));
                abort();
              }
            }
          }
        }

        usleep(1000);
      }

      uint32_t no_prepared{};
      uint32_t no_transmitted{};

      for (uint32_t c = 0; c < no_connections; ++c) {
        auto &conn = connections[c];
        auto &intf = interfaces[conn.interface_id];

        conn.resource_id = intf.resource_id++;
        const auto [vi_id, buf_id] = split_resource_id(conn.resource_id);
        auto &vi = intf.vis[vi_id];

        const uint32_t tx_offset = buf_id * tx_buf_size;
        conn.zfds.headers = vi.tx_mem + tx_offset;
        conn.zfds.headers_size = ETH_HLEN + IP_TCP_HEADERS;

        if (const auto rc = zf_delegated_send_prepare(conn.zock, msg_len * 2, 0,
                                                      0, &conn.zfds);
            rc != ZF_DELEGATED_SEND_RC_OK) {
          fprintf(stderr,
                  "zf_delegated_send_prepare, rc: [%d], errno: [%d/%s]\n",
                  int(rc), errno, strerror(errno));
          abort();
        }

        [[maybe_unused]] const auto *seq_p = reinterpret_cast<uint32_t *>(
            reinterpret_cast<uintptr_t>(conn.zfds.headers) +
            conn.zfds.tcp_seq_offset);

        // fprintf(stderr, "conn: [%u], seq: [%u]\n", c, ntohl(*seq_p));

        uint32_t allowed_to_send =
            std::min(conn.zfds.delegated_wnd, conn.zfds.mss);

        if (msg_len <= allowed_to_send) {
          conn.pio_pkt_len = conn.zfds.headers_len + msg_len;
          memset(conn.data(), '0' + c, msg_len);
          zf_delegated_send_tcp_update(&conn.zfds, msg_len,
                                       true); // true - PUSH

          TRY(ef_pio_memcpy(&vi.vi, conn.zfds.headers, tx_offset,
                            conn.pio_pkt_len));
          ++no_prepared;
        } else {
          TRY(zf_delegated_send_cancel(conn.zock));
          conn.zfds.delegated_wnd = 0;
          conn.pio_pkt_len = 0;
          abort();
        }
      }

      const uint64_t tsc = __rdtsc();

      for (uint32_t c = 0; c < no_connections; c += 2)
        no_transmitted += smart_transmit(connections[c], interfaces);

      for (uint32_t c = 1; c < no_connections; c += 2)
        no_transmitted += smart_transmit(connections[c], interfaces);

      /* for (auto& intf : interfaces) {
          if (intf.last_used_vi) {
              ef10_pio_push(intf.last_used_vi);
              intf.last_used_vi = nullptr;
          }
      } */

      const uint64_t elapsed_tsc = __rdtsc() - tsc;

      for (uint32_t c = 0; !immediate_complete && c < no_connections; ++c) {
        auto &conn = connections[c];

        if (!conn.pio_pkt_len)
          continue;

        conn.pio_pkt_len = 0;

        iovec iov{};
        iov.iov_base = conn.data();
        iov.iov_len = msg_len;
        TRY(zf_delegated_send_complete(conn.zock, &iov, 1, 0));
      }

      fprintf(stdout, "tsc: [%lu]\n", elapsed_tsc);

      // fprintf(stderr, "prepared: [%u], transmitted: [%u]\n", no_prepared,
      // no_transmitted);
    }

    return 0;
}