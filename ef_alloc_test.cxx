#include "utils.h"

#include <zf/zf.h>
#include <etherfabric/vi.h>
#include <etherfabric/pio.h>
#include <etherfabric/pd.h>
#include <etherfabric/memreg.h>
#include <etherfabric/capabilities.h>
#include "checksum.h"
#include <ci/tools.h>
#include <ci/tools/ippacket.h>

#include <stddef.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <pthread.h>
#include <x86intrin.h>

#include <cstdint>
#include <cstdio>

using ef_vi_flags_t = enum ef_vi_flags;

int main() {

    ef_pd pd;
    ef_pio pio;
    ef_driver_handle dh;
    ef_vi vi[64]{};

    int ifindex = if_nametoindex("sf_mm");

    if (!ifindex) {
        fprintf(stderr, "failed if_nametoindex\n");
        return EXIT_FAILURE;
    }

    TRY(ef_driver_open(&dh));

    unsigned long capability_val{};

    if (ef_vi_capabilities_get(dh, ifindex, EF_VI_CAP_TX_ALTERNATIVES, &capability_val)) {
        fprintf(stderr, "EF_VI_CAP_TX_ALTERNATIVES failed\n");
        return EXIT_FAILURE;
    }

    fprintf(stderr, "EF_VI_CAP_TX_ALTERNATIVES: [%lu]\n", capability_val);

    if (ef_vi_capabilities_get(dh, ifindex, EF_VI_CAP_TX_ALTERNATIVES_CP_BUFFERS, &capability_val)) {
        fprintf(stderr, "EF_VI_CAP_TX_ALTERNATIVES_CP_BUFFERS failed\n");
        return EXIT_FAILURE;
    }

    fprintf(stderr, "EF_VI_CAP_TX_ALTERNATIVES_CP_BUFFERS: [%lu]\n", capability_val);

    if (ef_vi_capabilities_get(dh, ifindex, EF_VI_CAP_TX_ALTERNATIVES_CP_BUFFER_SIZE, &capability_val)) {
        fprintf(stderr, "EF_VI_CAP_TX_ALTERNATIVES_CP_BUFFER_SIZE failed\n");
        return EXIT_FAILURE;
    }

    fprintf(stderr, "EF_VI_CAP_TX_ALTERNATIVES_CP_BUFFER_SIZE: [%lu]\n", capability_val);

    TRY(ef_pd_alloc(&pd, dh, ifindex, EF_PD_DEFAULT));

    const unsigned N_TX_ALT = 4;
    const unsigned BUF_SIZE = 512;

    for (int i = 0; i < 64; ++i) {
        TRY(ef_vi_alloc_from_pd(&vi[i], dh, &pd, dh, -1, -1,-1, nullptr, -1, ef_vi_flags_t(EF_VI_FLAGS_DEFAULT | EF_VI_TX_ALT)));

        /*
        TRY(ef_pio_alloc(&pio, dh, &pd, -1, dh));
        TRY(ef_pio_link_vi(&pio, dh, &vi[i], dh));
        */

        if (ef_vi_transmit_alt_alloc(&vi[i], dh, N_TX_ALT, N_TX_ALT * BUF_SIZE)) {
            fprintf(stderr, "alloc failed, i: [%d]\n", i);
            return EXIT_SUCCESS;
        }

        fprintf(stderr, "N_TX_ALT: [%u] allocated, i: [%d]\n", N_TX_ALT, i);
    }
}