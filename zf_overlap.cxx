#include "utils.h"
#include <zf/zf.h>
#include <cstdio>

struct rx_msg {
    struct zfur_msg msg;
    struct iovec iov[1];
};

static inline int getaddrinfo_hostport(const char* hostport_c, const struct addrinfo* hints, struct addrinfo** res) {
    char* hostport = strdup(hostport_c);
    if (!hostport)
        return EAI_MEMORY;

    char* port = strrchr(hostport, ':');

    if (port)
        *port++ = '\0';

    struct addrinfo hints2;

    if (!hints) {
        memset(&hints2, 0, sizeof(hints2));
        hints = &hints2;
    }

    int rc = getaddrinfo(hostport, port, hints, res);
    free(hostport);
    return rc;
}

int main(int argc, char* argv[]) {
    struct addrinfo *ai_local; // , *ai_remote;
    TRY(getaddrinfo_hostport(argv[1], nullptr, &ai_local));
    // TRY(getaddrinfo_hostport(argv[2], nullptr, &ai_remote));

    TRY(zf_init());

    struct zf_attr* attr;
    TRY(zf_attr_alloc(&attr));
    TRY(zf_attr_set_str(attr, "interface", "sf_mm"));
    TRY(zf_attr_set_int(attr, "reactor_spin_count", 1));
    // TRY(zf_attr_set_int(attr, "rx_timestamping", 1));

    struct zf_stack* stack;
    TRY(zf_stack_alloc(attr, &stack));

    struct zf_muxer_set* muxer;
    TRY(zf_muxer_alloc(stack, &muxer));

    struct zfur* zfur;
    TRY(zfur_alloc(&zfur, stack, attr));
    // TRY(zfur_addr_bind(zfur, ai_local->ai_addr, ai_local->ai_addrlen, ai_remote->ai_addr, ai_remote->ai_addrlen, 0));
    TRY(zfur_addr_bind(zfur, ai_local->ai_addr, ai_local->ai_addrlen, nullptr, 0, 0));

    struct epoll_event event{};
    event.events = EPOLLIN | EPOLLERR | ZF_EPOLLIN_OVERLAPPED;

    TRY(zf_muxer_add(muxer, zfur_to_waitable(zfur), &event));

    rx_msg msg;

    for (;;) {
        TRY(zf_muxer_wait(muxer, &event, 1, -1) == 1);

        if (event.events & EPOLLIN) {
            msg.msg.iovcnt = 1;
            zfur_zc_recv(zfur, &msg.msg, 0);
            assert(msg.msg.iovcnt == 1);
            fprintf(stderr, "EPOLLIN len: [%lu]\n", msg.msg.iov[0].iov_len);
            zfur_zc_recv_done(zfur, &msg.msg);
        }
        else if (event.events & ZF_EPOLLIN_OVERLAPPED) {
            msg.msg.iovcnt = 1;
            msg.msg.iov[0].iov_len = 64;

            zfur_zc_recv(zfur, &msg.msg, ZF_OVERLAPPED_WAIT);
            fprintf(stderr, "OVERLAPPED_WAIT len: [%lu], dgrams: [%d], cnt: [%d]\n", msg.msg.iov[0].iov_len, msg.msg.dgrams_left, msg.msg.iovcnt);

            if (msg.msg.iovcnt == 0)
                /* overlapped wait has been abandoned */
                continue;

            msg.msg.iov[0].iov_len = 128;
            zfur_zc_recv(zfur, &msg.msg, ZF_OVERLAPPED_WAIT);
            fprintf(stderr, "OVERLAPPED_WAIT - second len: [%lu], dgrams: [%d], cnt: [%d]\n", msg.msg.iov[0].iov_len, msg.msg.dgrams_left, msg.msg.iovcnt);

            if (msg.msg.iovcnt == 0)
                /* overlapped wait has been abandoned */
                continue;

            zfur_zc_recv(zfur, &msg.msg, ZF_OVERLAPPED_COMPLETE);
            fprintf(stderr, "OVERLAPPED_COMPLETE len: [%lu]\n", msg.msg.iov[0].iov_len);

            if (msg.msg.iovcnt == 0)
                /* packet did not pass verification */
                continue;

            zfur_zc_recv_done(zfur, &msg.msg);
        }
    }
}